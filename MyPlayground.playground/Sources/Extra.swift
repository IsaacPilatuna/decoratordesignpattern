import Foundation

public protocol Extra{
    func obtenerDescripcion()->String
    func obtenerValor()->Float
    
}
