import Foundation

protocol Platillo{
    var extras:[Extra] { get }
    func obtenerDescripcion()->String
    func obtenerValor()->Float
}
