import Foundation

public class Papas:Platillo{
    var extras: [Extra]
    
    
    public init(extras:[Extra]){
        self.extras=extras
    }
    
    public func obtenerDescripcion() -> String {
        let values=self.extras.map { return $0.obtenerDescripcion()}
        return "Papas" + values.reduce(" con ") {text, desc in "\(text),\(desc)" }
    }
    
    public func obtenerValor() -> Float {
        let values=self.extras.map { return $0.obtenerValor()}
        return 1.00 + values.reduce(0.0,+)
    }
    
    
}
